import { useState } from "react";

type UseInputProps = {
  defaultValue?: string;
};

const useInput = ({ defaultValue = "" }: UseInputProps) => {
  const [value, setValue] = useState<string>(defaultValue);

  const handleChange = (newValue: string) => {
    setValue(newValue);
  };

  return { value, handleChange };
};

export default useInput;
