import './App.css';
import { AuthPage } from './pages/auth/Auth.page';
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from 'react-router-dom';
import { PostsPage } from './pages/posts/list/Posts.page';
import { PostDetailsPage } from './pages/posts/details/PostDetails.page';
import { PostCreatePage } from './pages/posts/create/PostCreate.page';
import { PostEditPage } from './pages/posts/edit/PostEdit.page';

import Login from './components/auth/login/Login';
import { Header } from './components/layout/header/Header';
import { UserContextProvider } from './providers/UserContext';

const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route path="/" element={<PostCreatePage />} />
      <Route path="/login" element={<Login />} />
      <Route path="/posts/create" element={<PostCreatePage />} />
      <Route path="/posts/edit/:id" element={<PostEditPage />} />
      <Route path="/posts/:id" element={<PostDetailsPage />} />
      <Route path="/posts" element={<PostsPage />} />
    </>
  )
);

function App() {
  return (
    <UserContextProvider>
      <div className="App">
        <Header />
        <div className="App_content">
          <RouterProvider router={router} />
        </div>
      </div>
    </UserContextProvider>
  );
}

export default App;
