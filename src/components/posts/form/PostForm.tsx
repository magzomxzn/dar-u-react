import React, { FormEvent, useEffect, useState } from "react";
import { PostFormValues } from "../../../types/posts";
import { FormInput } from "../../ui/form-input/FormInput";

import { Controller, useForm, useWatch } from "react-hook-form";

type Props = {
  initialValues?: PostFormValues;
  isLoading: boolean;
  onSubmit: (data: PostFormValues) => void;
};

const defaultValues: PostFormValues = {
  title: "",
  body: "",
  userId: 0,
};

export const PostForm: React.FC<Props> = ({ initialValues, isLoading, onSubmit }) => {
  const {
    // register,
    control,
    handleSubmit,
  } = useForm<PostFormValues>({ defaultValues });
  const titleValue = useWatch({ name: "title", control });
  const authorValue = useWatch({ name: "userId", control });

  useEffect(() => {
    console.log({ titleValue });
  }, [titleValue]);

  const handleSuccess = (values: PostFormValues) => {
    console.log(values);
  };

  return (
    <form onSubmit={handleSubmit(handleSuccess)}>
      <h3>Post author: {authorValue}</h3>
      {/* <input {...register("name")} /> */}
      <Controller
        control={control}
        name="title"
        render={({ field: { onChange, value, name } }) => (
          <FormInput label="Title" value={value} name={name} onChange={onChange} />
        )}
      />
      <Controller
        control={control}
        name="body"
        render={({ field: { onChange, value, name } }) => (
          <FormInput label="Body" value={value} name={name} onChange={onChange} />
        )}
      />
      <Controller
        control={control}
        name="userId"
        render={({ field: { onChange, value, name } }) => (
          <FormInput label="User ID" value={`${value}`} name={name} onChange={onChange} />
        )}
      />
      {/* {error && <div>{error}</div>} */}
      <button type="submit" disabled={isLoading}>
        Save
      </button>
    </form>
  );
};
