import useInput from '../../../hooks/useInput';
import { FormInput } from '../../ui/form-input/FormInput';
import { Controller, useForm, useWatch } from 'react-hook-form';
import { FormEvent, useState } from 'react';
import { authService } from '../../..';
import { useUserContext } from '../../../providers/UserContext';

type Props = {};

type SignUpForm = {
  username: string;
  password: string;
};

type VerifyForm = {
  otp: string;
};

type LoginForm = {
  username: string;
  password: string;
};

const Login = (props: Props) => {
  const [sid, setSid] = useState('');
  const { changeUsername } = useUserContext();
  const { register, getValues } = useForm<SignUpForm>({
    defaultValues: {
      username: '',
      password: '',
    },
  });

  const { register: verifyRegister, getValues: verifyGetValues } =
    useForm<VerifyForm>({
      defaultValues: {
        otp: '',
      },
    });

  const { register: loginRegister, getValues: loginGetValues } =
    useForm<LoginForm>({
      defaultValues: {
        username: '',
        password: '',
      },
    });

  const handleSignupSubmit = (e: FormEvent) => {
    e.preventDefault();
    const username = getValues('username');
    console.log(username);
    authService
      .signUp(username)
      .then((r) => {
        setSid(r.sid);
      })
      .then((r) => {
        console.log(r);
      });
  };

  const handleVerifySubmit = (e: FormEvent) => {
    e.preventDefault();
    const otp = verifyGetValues('otp');
    console.log(otp);
    authService
      .sendOtp(otp, sid)
      .then((r) => {
        console.log(r);
        return authService.register(sid, getValues('password'));
      })
      .then((r) => {
        console.log(r);
        authService.persistTokens(r);
      });
  };

  const handleLoginSubmit = (e: FormEvent) => {
    e.preventDefault();
    authService
      .login({
        password: loginGetValues('password'),
        username: loginGetValues('username'),
      })
      .then((r) => {
        authService.persistTokens(r);
        return authService.getProfile();
      })
      .then((r) => {
        authService.persistProfile(r);
        changeUsername(r.email);
      })
      .catch((e) => {
        alert(e.response?.data?.message);
      });
  };
  return (
    <>
      <form className="Register__form" onSubmit={handleSignupSubmit}>
        <label>Username</label>
        <input type="email" {...register('username')} />
        <label>Password</label>
        <input type="password" {...register('password')} />
        <button type="submit">Save</button>
      </form>
      <form className="Verify__form" onSubmit={handleVerifySubmit}>
        <label>OTP</label>
        <input type="text" {...verifyRegister('otp')} />
        <button type="submit">Save</button>
      </form>
      <form className="Login__form" onSubmit={handleLoginSubmit}>
        <label>Username</label>
        <input type="email" {...loginRegister('username')} />
        <label>Password</label>
        <input type="password" {...loginRegister('password')} />
        <button type="submit">Save</button>
      </form>
    </>
  );
};

export default Login;
