import { useUserContext } from '../../../providers/UserContext';
import styles from './Header.module.scss';

type Props = {};

export const Header: React.FC<Props> = () => {
  const { username, changeUsername } = useUserContext();

  const handleUsernameClick = () => {
    changeUsername('Miras Magzom');
  };

  return (
    <div className={styles.header}>
      <div className={styles.profile_info} onClick={handleUsernameClick}>
        {username}
      </div>
    </div>
  );
};
