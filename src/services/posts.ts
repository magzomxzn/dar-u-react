import axios from 'axios';
import { Post, PostFormValues } from '../types/posts';

const { REACT_APP_API_ROOT } = process.env;

export const getPosts = () => {
  return axios
    .get<Post[]>(`${REACT_APP_API_ROOT}/posts`, {})
    .then((res) => res.data);
};

export const getPost = (id: number) => {
  return axios
    .get<Post>(`${REACT_APP_API_ROOT}/posts/${id}`, {})
    .then((res) => res.data);
};

export const createPost = (data: PostFormValues) => {
  return axios
    .post(`${REACT_APP_API_ROOT}/posts`, data)
    .then((res) => res.data);
};

export const updatePost = (id: number, data: PostFormValues) => {
  return axios
    .put(`${REACT_APP_API_ROOT}/posts/${id}`, data)
    .then((res) => res.data);
};

interface Event {
  title: string;
  image: string;
}

interface BackEvent {
  name: string;
  image: string;
}

const eventsAdapter = (event: BackEvent): Event => {
  return {
    title: event.name,
    image: event.image,
  };
};

export const getEvents = (): Promise<Event[]> => {
  return axios.get<BackEvent[]>('').then((r) => r.data.map(eventsAdapter));
};
